/****
	*
	* Browserify:
	*
	* 	browserify src/bundled-libs.src.js -o temp/bundled-libs.browserified.js
	*
	* Babel + UglifyJS (for .min.js):
	* 
	* 	babel temp/bundled-libs.browserified.js --out-file temp/bundled-libs.babeled.js
	* 	uglifyjs temp/bundled-libs.babeled.js --compress --mangle > dist/bundled-libs.min.js
	*
	* Piped:
	*	browserify src/bundled-libs.src.js -o temp/bundled-libs.browserified.js && babel temp/bundled-libs.browserified.js --out-file temp/bundled-libs.babeled.js --presets=env && uglifyjs temp/bundled-libs.babeled.js --compress --mangle > dist/bundled-libs.min.js
	*
	*/
	
'use strict';
require('pixi.js');
require('pixi-tween');
require('pixi-sound');
window._ = require('lodash');
window.EJSON = require('ejson');
window.SAT = require('sat');