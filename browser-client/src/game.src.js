/****
	*
	* Babel + UglifyJS (for .min.js):
	* 
	* 	babel src/game.src.js --out-file temp/game.babeled.js
	* 	uglifyjs temp/game.babeled.js --compress --mangle > dist/game.min.js
	*
	* Piped(Development):
	*	babel src/game.src.js --out-file dist/game.min.js --presets=env
	*
	* Piped(Production):
	*	babel src/game.src.js --out-file temp/game.babeled.js --presets=env && uglifyjs temp/game.babeled.js --compress booleans,comparisons,conditionals,loops,hoist_funs,properties,drop_console --mangle toplevel,eval > dist/game.min.js
	*
	*/

'use strict';

PIXI.settings.GC_MAX_CHECK_COUNT = 300;
PIXI.settings.GC_MAX_IDLE = 30;

var windowWidth = window.innerWidth * 0.95;
var windowHeight = 480;
var windowRatio = windowWidth / windowHeight;

// Set-up our renderer view.
var PIXI_APP = new PIXI.Application();
PIXI_APP.renderer = new PIXI.CanvasRenderer(
	windowWidth,
	windowHeight,
	{
		antialias: true,
		transparent: false,
		resolution: 1,
		autoResize: true
	}
);
PIXI_APP.renderer.view.style.border = "1px dashed black";
PIXI_APP.renderer.backgroundColor = 0xFFFFFF;
//PIXI_APP.renderer.view.style.position = "static";
//PIXI_APP.renderer.view.style.display = "block";
var PIXI_DIV = document.getElementById("PIXI_DIV");
PIXI_DIV.appendChild(PIXI_APP.view);

/*
function resize() {
    if (window.innerWidth / window.innerHeight >= windowRatio) {
        var w = window.innerHeight * windowRatio;
        var h = window.innerHeight;
    } else {
        var w = window.innerWidth;
        var h = window.innerWidth / windowRatio;
    }
    PIXI_APP.renderer.view.style.width = w * 0.95 + 'px';
    PIXI_APP.renderer.view.style.height = h * 0.95 + 'px';
}
window.onresize = resize;
resize();
*/

// CONTAiNERS

var SceneContainer = new PIXI.Container();
PIXI_APP.stage.addChild(SceneContainer);

var TurretsContainer = new PIXI.Container();
PIXI_APP.stage.addChild(TurretsContainer);

var TanksContainer = new PIXI.Container();
PIXI_APP.stage.addChild(TanksContainer);

var GuiContainer = new PIXI.Container();
PIXI_APP.stage.addChild(GuiContainer);



// INTERACTION MANAGER

var interactionManager = new PIXI.interaction.InteractionManager(
	PIXI_APP.renderer,
	{
		autoPreventDefault: true
	}
);

interactionManager.defaultCursorStyle = "none";



var TileTypes = {
	winter1: "./tile_winter_1",
	winter2: "./tile_winter_2",
	winter3: "./tile_winter_3",
	winter11: "./tile_winter_11",
	winter12: "./tile_winter_12",
	winter13: "./tile_winter_13"
};

function initializeScene(){
	drawSceneElement(
		"tile_winter_bg",
		1, windowWidth / 2, windowHeight / 2
	);
	
	
	drawSceneElement(
		TileTypes.winter1,
		0.5, 64, 460
	);
	for(var i=2; i <= 19; i++){
		drawSceneElement(
			TileTypes.winter2,
			0.5, 64 * i, 460
		);
	}
	drawSceneElement(
		TileTypes.winter3,
		0.5, 64 * 20, 460
	);
	createRegion(
		windowWidth / 2, 460,
		64 * 20, 64
	);
	
	
	
	drawSceneElement(
		TileTypes.winter11,
		0.5, 256 + 64, 300
	);
	drawSceneElement(
		TileTypes.winter12,
		0.5, 256 + 64*2, 300
	);
	drawSceneElement(
		TileTypes.winter13,
		0.5, 256 + 64*3, 300
	);
	createRegion(
		256 + 64*2, 300 - 8,
		64 * 3, 32
	);
	
	
	
	drawSceneElement(
		TileTypes.winter11,
		0.5, 256*3 + 64, 300
	);
	drawSceneElement(
		TileTypes.winter12,
		0.5, 256*3 + 64*2, 300
	);
	drawSceneElement(
		TileTypes.winter13,
		0.5, 256*3 + 64*3, 300
	);
	createRegion(
		256*3 + 64*2, 300 - 8,
		64 * 3, 32
	);
	
	
	
}

var sceneRegions = [];

function drawSceneElement(param_elementType, param_scale, param_x, param_y){
	var ElementSprite = new PIXI.Sprite(
		PIXI.utils.TextureCache[param_elementType]
	);
	ElementSprite.anchor.set(0.5, 0.5);
	ElementSprite.scale.set(param_scale, param_scale);
	ElementSprite.position.set(param_x, param_y);
	SceneContainer.addChild(ElementSprite);
}

function createRegion(param_x, param_y, param_w, param_h){
	sceneRegions.push({
		x: param_x,
		y: param_y,
		w: param_w,
		h: param_h
	});
}





// CURSOR GUI
var CrosshairTypes = {
	typeOne: {
		texture: "./Crosshair (1)"
	},
	typeTwo: {
		texture: "./Crosshair (2)"
	},
	typeThree: {
		texture: "./Crosshair (3)"
	},
	typeFour: {
		texture: "./Crosshair (4)"
	}
};

function initializeCursor(){
	var CursorSprite = new PIXI.Sprite(
		PIXI.utils.TextureCache[CrosshairTypes.typeOne.texture]
	);
		
	CursorSprite.anchor.set(0.5, 0.5);
	CursorSprite.scale.set(0.4, 0.4);
	GuiContainer.addChild(CursorSprite);

	PIXI_APP.ticker.add(function(delta){
		CursorSprite.position.set(
			interactionManager.pointer.global.x,
			interactionManager.pointer.global.y
		);
	});
	/*
	setInterval(function(){
		var TempInteger = _.random(1,4);
		CursorSprite.texture = PIXI.utils.TextureCache[`./Crosshair (${TempInteger})`];
	}, 250);
	*/
}





// CreateSpriteRegion
// BindToSpriteOffset

// TankDot
// getBodyRegion

var sceneTanks = [];

var TankTypes = {
	DesertOne: {
		texture: "./Desert (1)",
		scale: 0.35,
		turret: {
			texture: "./Turret (1)",
			scale: 0.35,
			anchor: {
				x: 0.1,
				y: 0.5
			},
			offset: {
				x: 0,
				y: -16
			},
			bulletOriginOffset: 15
		},
		regionOffsetX: -2,
		regionOffsetY: 4,
		regionWidth: 48,
		regionHeight: 38
	}
};

function makeAimTankTurretToAngle(paramTank, param_angle){
	paramTank.Turret.rotation = param_angle * Math.PI / -180;
};

function makeTankMove(paramTank, param_position_x, param_position_y){
	paramTank.position.set(param_position_x, param_position_y);
	paramTank.Turret.position.set(
		param_position_x + paramTank.Turret.offset.x,
		param_position_y + paramTank.Turret.offset.y
	);
};

function makeSpriteFaceAngle(param_sprite, param_angle_in_degrees){
	param_sprite.rotation = param_angle_in_degrees * Math.PI / -180;
};

function makeTankFaceLeft(paramTank){
	paramTank.scale.set(-paramTank.type.scale, paramTank.type.scale);
	paramTank.facingDirection = "Left";
}

function makeTankFaceRight(paramTank){
	paramTank.scale.set(paramTank.type.scale, paramTank.type.scale);
	paramTank.facingDirection = "Right";
}

function getTankRegion(paramTank){
	return {
		x1: parseInt(paramTank.position.x + paramTank.regionOffsetX - paramTank.regionWidth / 2),
		y1: parseInt(paramTank.position.y + paramTank.regionOffsetY  - paramTank.regionHeight / 2),
		x2: parseInt(paramTank.position.x + paramTank.regionOffsetX  + paramTank.regionWidth / 2),
		y2: parseInt(paramTank.position.y + paramTank.regionOffsetY  + paramTank.regionHeight / 2),
		w: paramTank.regionWidth,
		h: paramTank.regionHeight
	}
}

var CreateTank = function(param_TankType, param_position_x, param_position_y){
	var TankSprite = new PIXI.Sprite(
		PIXI.utils.TextureCache[param_TankType.texture]
	);
	TankSprite.type = param_TankType;
	TankSprite.anchor.set(0.5, 0.5);
	TankSprite.scale.set(param_TankType.scale, param_TankType.scale);
	
	
	var TurretSprite = new PIXI.Sprite(
		PIXI.utils.TextureCache[param_TankType.turret.texture]
	);
	TurretSprite.scale.set(
		param_TankType.turret.scale, 
		param_TankType.turret.scale
	);
	TurretSprite.anchor.set(
		param_TankType.turret.anchor.x, 
		param_TankType.turret.anchor.y
	);
	makeSpriteFaceAngle(TurretSprite, 0);	
	TankSprite.Turret = TurretSprite;
	TankSprite.Turret.offset = param_TankType.turret.offset;
	
	TankSprite.regionOffsetX = param_TankType.regionOffsetX;
	TankSprite.regionOffsetY = param_TankType.regionOffsetY;
	TankSprite.regionWidth = param_TankType.regionWidth;
	TankSprite.regionHeight = param_TankType.regionHeight;
	

	TankSprite.motion = {
		move: function(param_X_Adjustment, param_Y_Adjustment, delta){
			
			var Final_X_Adjustment = param_X_Adjustment * delta;
			var Final_Y_Adjustment = param_Y_Adjustment * delta;
			
			var TempTankRegion = getTankRegion(TankSprite);
			var SAT_TankVector = new SAT.Vector(
				TempTankRegion.x1, TempTankRegion.y1
			);
			var SAT_TankRegionBox = new SAT.Box(
				SAT_TankVector,
				TempTankRegion.w, TempTankRegion.h
			).toPolygon();
			
			var collidedWithAnything = false;
			
			var Y_Overlap_Adjustment = 0;
			
			for(var regionIndex in sceneRegions){
				var TempSceneRegion = sceneRegions[regionIndex];
				var SAT_SceneVector = new SAT.Vector(
					parseInt(TempSceneRegion.x - (TempSceneRegion.w / 2)), 
					parseInt(TempSceneRegion.y - (TempSceneRegion.h / 2))
				);
				var SAT_SceneRegionBox = new SAT.Box(
					SAT_SceneVector,
					TempSceneRegion.w, TempSceneRegion.h
				).toPolygon();
				
				var TempSATResponse = new SAT.Response();
				if(SAT.testPolygonPolygon(SAT_TankRegionBox, SAT_SceneRegionBox, TempSATResponse)){
					collidedWithAnything = true;
					if(TempSATResponse.overlap){
						Y_Overlap_Adjustment = TempSATResponse.overlap;
					}
				}
			}
			
			if(collidedWithAnything){
				if(Final_Y_Adjustment > 0){
					Final_Y_Adjustment = 0;
					if(Y_Overlap_Adjustment){
						Final_Y_Adjustment -= Y_Overlap_Adjustment / 4;
					}
				}
			}
			
			TankSprite.position.set(
				TankSprite.position.x + Final_X_Adjustment, 
				TankSprite.position.y + Final_Y_Adjustment
			);
			TankSprite.Turret.position.set(
				TankSprite.position.x + TankSprite.Turret.offset.x,
				TankSprite.position.y + TankSprite.Turret.offset.y
			);
				
		}
	};
	
	
	TanksContainer.addChild(TankSprite);
	TurretsContainer.addChild(TurretSprite);
	makeTankMove(TankSprite, param_position_x, param_position_y);
	sceneTanks.push(TankSprite);
	return TankSprite;
};


function constructor_MotionManager(){
	
	console.log(`MotionManager :: Initializing!`);
	
	var TempTicker = new PIXI.ticker.Ticker();
	var TempFunctions = [];
	var TempManagedUnits = [];
	
	TempTicker.add(function(delta){
		
		if(TempManagedUnits.length > 0){
						
			for(var TempUnitIndex in TempManagedUnits){
				
				var Temp_Y_Adjustment = TempManagedUnits[TempUnitIndex].motion.forces.down - TempManagedUnits[TempUnitIndex].motion.forces.up;
				
				var Temp_X_Adjustment = TempManagedUnits[TempUnitIndex].motion.forces.right - TempManagedUnits[TempUnitIndex].motion.forces.left;
				
				try{
					TempManagedUnits[TempUnitIndex].motion.move(Temp_X_Adjustment, Temp_Y_Adjustment, delta);
				}catch(e){
					console.log(`MotionManager :: ${e}`);
				}
				
			}
			
			
		}
		
	});
	
	TempTicker.start();
	
	// Self-Assignments
	this.ticker = TempTicker;
	this.functions = TempFunctions;
	this.managedUnits = TempManagedUnits;
	
	// Functions
	this.initializeUnitMotion = function(param_unit){
		
		if(param_unit.motion){
			
			param_unit.motion.forces = {
				up: 0,
				down: 8, // GRAVITY
				left: 0,
				right: 0
			}
			
			this.managedUnits.push(param_unit);
			
		}else{
			
			console.log(`MotionManager :: Error, can't initialize unit, missing motion property!`);
			
		}
		
	}
	
	
	this.applyConstantForce = function(param_unit, param_direction, param_value){
		// check if motion exists first, else error "must initialize unit's motion property first."
		if(param_unit.motion){
			param_unit.motion.forces[param_direction] += parseInt(param_value);
		}else{
			console.log(`MotionManager :: Error, must initialize unit's motion property first!`);
		}
	}
	
	this.applyDecayingForce = function(param_unit){
		// check if motion exists first, else error "must initialize unit's motion property first."
		// allow decay by value.. -1, -1, -1
		// by factor / percentage, - 5%, -5%, -5%, -5%
		// Use setInterval for decay.
		// Use cancelInterval when nothing else to decrement, or when it's already ZERO or BELOW ZERO.
	}
	
	
	return this;
}

var MotionManager = new constructor_MotionManager();
	
var KeyPressManager = new keypress.Listener();

function RegularHotkey(keys, param_fnKeyDown, param_fnKeyUp){
	
	//var TempTicker = new PIXI.ticker.Ticker();
	//TempTicker.add(param_function);
	KeyPressManager.register_combo({
		"keys"              : keys,
		"on_keydown"        : function(){
			//TempTicker.start();
			param_fnKeyDown();
		},
		"on_keyup"          : function(){
			//TempTicker.stop();
			param_fnKeyUp();
		},
		"on_release"        : null,
		"this"              : undefined,
		"prevent_default"   : true,
		"prevent_repeat"    : true,
		"is_unordered"      : false,
		"is_counting"       : false,
		"is_exclusive"      : false,
		"is_solitary"       : false,
		"is_sequence"       : false
	});
}

function PixiAppHotkey(keys, param_function){
	
	var TempTicker = new PIXI.ticker.Ticker();
	TempTicker.add(param_function);
	KeyPressManager.register_combo({
		"keys"              : keys,
		"on_keydown"        : function(){
			TempTicker.start();
		},
		"on_keyup"          : function(){
			TempTicker.stop();
		},
		"on_release"        : null,
		"this"              : undefined,
		"prevent_default"   : true,
		"prevent_repeat"    : true,
		"is_unordered"      : false,
		"is_counting"       : false,
		"is_exclusive"      : false,
		"is_solitary"       : false,
		"is_sequence"       : false
	});
}

console.log(`Loader :: Queueing assets.`);
PIXI.loader.add("./assets/spritesheets/sheet-1.json");
PIXI.loader.add("tile_winter_bg", "./assets/images/tile_winter_bg.png");

PIXI.loader.on("progress", function(loader, resource){
	console.log(`Loader :: Loading ${resource.url} - ${loader.progress}%`);
});

var TempTank = null;






class NewMotionManager{
	constructor(){
		
		/** Class Instantiated! **/
		console.log(`MotionManager :: Class instantiated!`);
		
		/** Temp Vars **/
		var TempTicker = new PIXI.ticker.Ticker();
		var TempFunctions = [];
		var TempManagedEntities = [];
		
		/** Creation of the Ticker **/
		TempTicker.add(function(param_ticker_delta){			
			if(TempManagedEntities.length > 0){							
				for(var TempUnitIndex in TempManagedEntities){
					
					if(TempManagedEntities[TempUnitIndex].motion.decay_value){
						if(TempManagedEntities[TempUnitIndex].motion.decay_value.up){
							
							if(TempManagedEntities[TempUnitIndex].motion.forces.up > 0){
								TempManagedEntities[TempUnitIndex].motion.forces.up -= TempManagedEntities[TempUnitIndex].motion.decay_value.up;
							}
							
						}
					}
					
					var Temp_Total_X_Offset = TempManagedEntities[TempUnitIndex].motion.forces.right - TempManagedEntities[TempUnitIndex].motion.forces.left;
					
					var Temp_Total_Y_Offset = TempManagedEntities[TempUnitIndex].motion.forces.down - TempManagedEntities[TempUnitIndex].motion.forces.up;
					
					try{
						TempManagedEntities[TempUnitIndex].ApplyMotion(Temp_Total_X_Offset, Temp_Total_Y_Offset, param_ticker_delta);
					}catch(e){
						console.log(`MotionManager :: ${e}`);
					}
					
				}	
			}
		});
		
		TempTicker.start();
		
		/** Self-Assignments **/
		this.ticker = TempTicker;
		this.functions = TempFunctions;
		this.managedEntities = TempManagedEntities;
	}
	manageEntity(param_entity){
		this.managedEntities.push(param_entity);
	}
}

class Projectile{
	constructor(options){
		// Options: projectile type, position {x, y}, facing angle.
		// Other options: sourceUnit
		// Create the sprite
		// Create the region
		// Initialize this unit's motion.
		
		
		var TempProjectileSprite = new PIXI.Sprite(
			PIXI.utils.TextureCache[options.type.texture]
		);
		TempProjectileSprite.anchor.set(0.5, 0.5);
		TempProjectileSprite.scale.set(options.type.scale, options.type.scale);
		TempProjectileSprite.position.set(options.position.x, options.position.y);
		
		TempProjectileSprite.rotation = options.firing_angle * Math.PI / -180;
		
		options.pixi_stage.addChild(TempProjectileSprite);
		
		this.sprite = TempProjectileSprite;
		
		this.type = options.type;
				
		var temp_up = 0;
		var temp_left = 0;
		var temp_right = 0;
		
		var temp_x_offset = parseInt(
			parseFloat(
				20 * Math.cos(options.firing_angle * Math.PI / 180)
			).toFixed(2)
		);
		var temp_y_offset = parseInt(
			parseFloat(
				20 * Math.sin(options.firing_angle * Math.PI / 180)
			).toFixed(2)
		);
		
		if(temp_x_offset > 0){
			temp_right = Math.abs(temp_x_offset);
		}else if(temp_x_offset < 0){
			temp_left = Math.abs(temp_x_offset);
		}
		
		temp_up = Math.abs(temp_y_offset);
		
		this.motion = {
			decay_value: {
				up: 0.33,
				left: 1,
				right: 1
			},
			forces: {
				up: 6 + temp_up,
				down: 6, // gravity
				left: temp_left,
				right: temp_right
			}
		};
		
		options.motion_manager.manageEntity(this);
	}
	ApplyMotion(param_offset_x, param_offset_y, param_ticker_delta){
		// calculate angle, face angle, move projectile.
		// Either applies the movement.
		// Explodes if it touches anything.
		
		var TempAngleDeg = Math.atan2(
		(this.sprite.position.y + (param_offset_y * param_ticker_delta)) - this.sprite.position.y,
		(this.sprite.position.x + (param_offset_x * param_ticker_delta)) - this.sprite.position.x,
		) * 180 / Math.PI;
		
		if(TempAngleDeg < 0 ){
			TempAngleDeg = +Math.abs(TempAngleDeg);
		}else if(TempAngleDeg > 0 ){
			TempAngleDeg = -Math.abs(TempAngleDeg);
		}
		
		this.sprite.rotation = TempAngleDeg * Math.PI / -180;
		
		this.sprite.position.set(
			this.sprite.position.x + (param_offset_x * param_ticker_delta),
			this.sprite.position.y + (param_offset_y * param_ticker_delta)
		);
	}
	static Types(){
		return {
			RegularBullet : {
				texture: "./Bullets (1)",
				scale: 0.25,
				regionOffsetX: -2,
				regionOffsetY: 4,
				regionWidth: 48,
				regionHeight: 38
			}
		};
	}	
}







PIXI.loader.load(function(loader,resources){
		
	console.log(`Loader :: All assets loaded!`);
	
	initializeCursor();
	
	initializeScene();
	
	TempTank = CreateTank(TankTypes.DesertOne,
		windowWidth / 2,
		windowHeight * 0.8
	);
	
	
	MotionManager.initializeUnitMotion(TempTank);
	//MotionManager.applyConstantForce(TempTank, "down", 1);
	
	RegularHotkey("a",
		function(){
			TempTank.motion.forces.left += 1;
			TempTank.motion.forces.up += 1;
		},
		function(){
			TempTank.motion.forces.left -= 1;
			TempTank.motion.forces.up -= 1;
		}
	);
	
	RegularHotkey("d",
		function(){
			TempTank.motion.forces.right += 1;
			TempTank.motion.forces.up += 1;
		},
		function(){
			TempTank.motion.forces.right -= 1;
			TempTank.motion.forces.up -= 1;
		}
	);
	
	RegularHotkey("w",
		function(){
			TempTank.motion.forces.up += 10;
		},
		function(){
			TempTank.motion.forces.up -= 10;
		}
	);
		
	interactionManager.on("mousedown", function(data){
		console.log(`Clicked mouse at ${parseInt(data.data.global.x)}, ${parseInt(data.data.global.y)}`);
		var TempAngleDeg = Math.atan2(data.data.global.y - TempTank.Turret.position.y, data.data.global.x - TempTank.Turret.position.x) * 180 / Math.PI;
		
		if(TempAngleDeg < 0 ){
			TempAngleDeg = +Math.abs(TempAngleDeg);
		}else if(TempAngleDeg > 0 ){
			TempAngleDeg = -Math.abs(TempAngleDeg);
		}
		
		console.log(`Angle: ${TempAngleDeg}`);
		
		console.log(TempTank.Turret.rotation / Math.PI * 180);
		
		var TempProjectile = new Projectile({
			type: Projectile.Types().RegularBullet,
			position: {x: TempTank.Turret.position.x, y: TempTank.Turret.position.y},
			firing_angle: TempAngleDeg,
			motion_manager: new NewMotionManager(),
			pixi_stage: PIXI_APP.stage 
		});
		
		// Create projectile here		
		// Throttle the projectile function
		// Assign angular motion force.
		// 
		
	});
	
		
	PIXI_APP.ticker.add(function(delta){
		var TempAngleDeg = Math.atan2(interactionManager.pointer.global.y - TempTank.Turret.position.y, interactionManager.pointer.global.x - TempTank.Turret.position.x) * 180 / Math.PI;
		
		if(TempAngleDeg < 0 ){
			TempAngleDeg = +Math.abs(TempAngleDeg);
		}else if(TempAngleDeg > 0 ){
			TempAngleDeg = -Math.abs(TempAngleDeg);
		}
				
		if( (0 < TempAngleDeg && TempAngleDeg < 90) || (-90 < TempAngleDeg && TempAngleDeg < 0) ){
			if(TempTank.facingDirection != "Right"){
				makeTankFaceRight(TempTank);
				TempTank.regionOffsetX = -Math.abs(TempTank.regionOffsetX);
			}
		}else{
			if(TempTank.facingDirection != "Left"){
				makeTankFaceLeft(TempTank);
				TempTank.regionOffsetX = Math.abs(TempTank.regionOffsetX);
			}
		}
		
		if(TempAngleDeg > 0){
			makeAimTankTurretToAngle(TempTank, TempAngleDeg);
		}else{
			switch(TempTank.facingDirection){
				case "Left":
					makeAimTankTurretToAngle(TempTank, 180);
					break;
				case "Right":
					makeAimTankTurretToAngle(TempTank, 0);
					break;
				default:
					break;
			}
		}
		
	});
	/*
	setInterval(function(){
		SceneContainer.position.set(
			SceneContainer.position.x,
			SceneContainer.position.y - 2
		);
		for(var regionIndex in sceneRegions){
			sceneRegions[regionIndex].y = sceneRegions[regionIndex].y - 2;
		}
	}, 300);
	*/
	Debug();
});




var Debug = function(){
	// Create tracer
	// Trace all regions w/ pixi ticker..
	// tank bodies
	// projectile bodies
	// floors and walls
	var Tracer = new PIXI.Graphics();
	console.log(`Debug :: Tracer started.`);
	PIXI_APP.stage.addChild(Tracer);
	PIXI_APP.ticker.add(function(delta){
		Tracer.clear();
		for(var regionIndex in sceneRegions){
			
			Tracer.lineStyle(2, 0x77DD77, 1);
						
			Tracer.beginFill(0x77DD77, 1);
			Tracer.drawCircle(
				sceneRegions[regionIndex].x,
				sceneRegions[regionIndex].y,
				2
			);
			Tracer.endFill();
					
			Tracer.drawRect(
				parseInt(sceneRegions[regionIndex].x - (sceneRegions[regionIndex].w / 2)),
				parseInt(sceneRegions[regionIndex].y - (sceneRegions[regionIndex].h / 2)),
				parseInt(sceneRegions[regionIndex].w),
				parseInt(sceneRegions[regionIndex].h)
			);
		}
		
		for(var tankIndex in sceneTanks){
			
			Tracer.lineStyle(2, 0x77DD77, 1);
						
			Tracer.beginFill(0x77DD77, 1);
			Tracer.drawCircle(
				sceneTanks[tankIndex].x,
				sceneTanks[tankIndex].y,
				2
			);
			Tracer.endFill();
			
			var TempRect = getTankRegion(sceneTanks[tankIndex]);
			Tracer.drawRect(
				TempRect.x1,
				TempRect.y1,
				TempRect.w,
				TempRect.h
			);
		}
		
	});
	
	setTimeout(function(){
		PIXI_APP.stage.removeChild(Tracer);
		PIXI_APP.stage.addChild(Tracer);
	}, 100);
};





// PIXI-PARTICLES TEST:
function createEffect(){
	
	var emitterContainer = new PIXI.Container();
	PIXI_APP.stage.addChild(emitterContainer);
	
	var frames = [];
	frames.push(PIXI.Texture.fromImage("./assets/images/bullet1 (1).png"));
	frames.push(PIXI.Texture.fromImage("./assets/images/bullet1 (2).png"));
	frames.push(PIXI.Texture.fromImage("./assets/images/bullet1 (3).png"));
	frames.push(PIXI.Texture.fromImage("./assets/images/bullet1 (4).png"));
	
	var config = {
					"alpha": {
						"start": 1,
						"end": 0.25
					},
					"scale": {
						"start": 0.2,
						"end": 0.2
					},
					"color": {
						"start": "FFFFFF",
						"end": "FFFFFF"
					},
					"speed": {
						"start": 700,
						"end": 500
					},
					"startRotation": {
						"min": -85,
						"max": -95
					},
					"rotationSpeed": {
						"min": 0,
						"max": 0
					},
					"lifetime": {
						"min": 0.5,
						"max": 0.5	
					},
					"frequency": 0.02,
					"emitterLifetime": 4,
					"maxParticles": 100,
					"pos": {
						"x": 0,
						"y": 0
					},
					"addAtBack": false,
					"spawnType": "point",
					"spawnCircle": {
						"x": 0,
						"y": 0,
						"r": 10
					},
					emit: true,
					autoUpdate: true
				};
	
	var ArtData = {
		framerate: 8,
		loop: true,
		textures: frames
	};
	
	var emitter = new PIXI.particles.Emitter(
		emitterContainer,
		frames,
		config
	);
	
	emitter.updateOwnerPos(window.innerWidth / 2, window.innerHeight / 2);
		
	setTimeout(function(){
		
				emitter.destroy();
				emitter = null;
				window.destroyEmitter = null;
				
				//reset SpriteRenderer's batching to fully release particles for GC
				if (PIXI_APP.renderer.plugins 
				&& PIXI_APP.renderer.plugins.sprite 
				&& PIXI_APP.renderer.plugins.sprite.sprites){
					PIXI_APP.renderer.plugins.sprite.sprites.length = 0;
				}
				
				PIXI_APP.renderer.render(PIXI_APP.stage);		
	}, 10000);
	
}