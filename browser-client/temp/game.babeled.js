/****
	*
	* Babel + UglifyJS (for .min.js):
	* 
	* 	babel src/game.src.js --out-file temp/game.babeled.js
	* 	uglifyjs temp/game.babeled.js --compress --mangle > dist/game.min.js
	*
	* Piped:
	*	babel src/game.src.js --out-file temp/game.babeled.js --presets=env && uglifyjs temp/game.babeled.js --compress booleans,comparisons,conditionals,loops,hoist_funs,properties,drop_console --mangle toplevel,eval > dist/game.min.js
	*
	*/

'use strict';

PIXI.settings.GC_MAX_CHECK_COUNT = 300;
PIXI.settings.GC_MAX_IDLE = 30;

var windowWidth = window.innerWidth * 0.95;
var windowHeight = 480;
var windowRatio = windowWidth / windowHeight;

// Set-up our renderer view.
var PIXI_APP = new PIXI.Application();
PIXI_APP.renderer = new PIXI.CanvasRenderer(windowWidth, windowHeight, {
	antialias: true,
	transparent: false,
	resolution: 1,
	autoResize: true
});
PIXI_APP.renderer.view.style.border = "1px dashed black";
PIXI_APP.renderer.backgroundColor = 0xFFFFFF;
//PIXI_APP.renderer.view.style.position = "static";
//PIXI_APP.renderer.view.style.display = "block";
var PIXI_DIV = document.getElementById("PIXI_DIV");
PIXI_DIV.appendChild(PIXI_APP.view);

/*
function resize() {
    if (window.innerWidth / window.innerHeight >= windowRatio) {
        var w = window.innerHeight * windowRatio;
        var h = window.innerHeight;
    } else {
        var w = window.innerWidth;
        var h = window.innerWidth / windowRatio;
    }
    PIXI_APP.renderer.view.style.width = w * 0.95 + 'px';
    PIXI_APP.renderer.view.style.height = h * 0.95 + 'px';
}
window.onresize = resize;
resize();
*/

// CONTAiNERS

var SceneContainer = new PIXI.Container();
PIXI_APP.stage.addChild(SceneContainer);

var TurretsContainer = new PIXI.Container();
PIXI_APP.stage.addChild(TurretsContainer);

var TanksContainer = new PIXI.Container();
PIXI_APP.stage.addChild(TanksContainer);

var GuiContainer = new PIXI.Container();
PIXI_APP.stage.addChild(GuiContainer);

// INTERACTION MANAGER

var interactionManager = new PIXI.interaction.InteractionManager(PIXI_APP.renderer, {
	autoPreventDefault: true
});

interactionManager.defaultCursorStyle = "none";

var TileTypes = {
	winter1: "./tile_winter_1",
	winter2: "./tile_winter_2",
	winter3: "./tile_winter_3",
	winter11: "./tile_winter_11",
	winter12: "./tile_winter_12",
	winter13: "./tile_winter_13"
};

function initializeScene() {
	drawSceneElement("tile_winter_bg", 1, windowWidth / 2, windowHeight / 2);

	drawSceneElement(TileTypes.winter1, 0.5, 64, 460);
	for (var i = 2; i <= 19; i++) {
		drawSceneElement(TileTypes.winter2, 0.5, 64 * i, 460);
	}
	drawSceneElement(TileTypes.winter3, 0.5, 64 * 20, 460);
	createRegion(windowWidth / 2, 460, 64 * 20, 64);

	drawSceneElement(TileTypes.winter11, 0.5, 256 + 64, 300);
	drawSceneElement(TileTypes.winter12, 0.5, 256 + 64 * 2, 300);
	drawSceneElement(TileTypes.winter13, 0.5, 256 + 64 * 3, 300);
	createRegion(256 + 64 * 2, 300 - 8, 64 * 3, 32);

	drawSceneElement(TileTypes.winter11, 0.5, 256 * 3 + 64, 300);
	drawSceneElement(TileTypes.winter12, 0.5, 256 * 3 + 64 * 2, 300);
	drawSceneElement(TileTypes.winter13, 0.5, 256 * 3 + 64 * 3, 300);
	createRegion(256 * 3 + 64 * 2, 300 - 8, 64 * 3, 32);
}

var sceneRegions = [];

function drawSceneElement(param_elementType, param_scale, param_x, param_y) {
	var ElementSprite = new PIXI.Sprite(PIXI.utils.TextureCache[param_elementType]);
	ElementSprite.anchor.set(0.5, 0.5);
	ElementSprite.scale.set(param_scale, param_scale);
	ElementSprite.position.set(param_x, param_y);
	SceneContainer.addChild(ElementSprite);
}

function createRegion(param_x, param_y, param_w, param_h) {
	sceneRegions.push({
		x: param_x,
		y: param_y,
		w: param_w,
		h: param_h
	});
}

// CURSOR GUI
var CrosshairTypes = {
	typeOne: {
		texture: "./Crosshair (1)"
	},
	typeTwo: {
		texture: "./Crosshair (2)"
	},
	typeThree: {
		texture: "./Crosshair (3)"
	},
	typeFour: {
		texture: "./Crosshair (4)"
	}
};

function initializeCursor() {
	var CursorSprite = new PIXI.Sprite(PIXI.utils.TextureCache[CrosshairTypes.typeOne.texture]);

	CursorSprite.anchor.set(0.5, 0.5);
	CursorSprite.scale.set(0.4, 0.4);
	GuiContainer.addChild(CursorSprite);

	PIXI_APP.ticker.add(function (delta) {
		CursorSprite.position.set(interactionManager.pointer.global.x, interactionManager.pointer.global.y);
	});
	/*
 setInterval(function(){
 	var TempInteger = _.random(1,4);
 	CursorSprite.texture = PIXI.utils.TextureCache[`./Crosshair (${TempInteger})`];
 }, 250);
 */
}

// CreateSpriteRegion
// BindToSpriteOffset

// TankDot
// getBodyRegion

var sceneTanks = [];

var TankTypes = {
	DesertOne: {
		texture: "./Desert (1)",
		scale: 0.35,
		turret: {
			texture: "./Turret (1)",
			scale: 0.35,
			anchor: {
				x: 0.1,
				y: 0.5
			},
			offset: {
				x: 0,
				y: -16
			},
			bulletOriginOffset: 15
		},
		regionOffsetX: -2,
		regionOffsetY: 4,
		regionWidth: 48,
		regionHeight: 38
	}
};

function makeAimTankTurretToAngle(paramTank, param_angle) {
	paramTank.Turret.rotation = param_angle * Math.PI / -180;
};

function makeTankMove(paramTank, param_position_x, param_position_y) {
	paramTank.position.set(param_position_x, param_position_y);
	paramTank.Turret.position.set(param_position_x + paramTank.Turret.offset.x, param_position_y + paramTank.Turret.offset.y);
};

function makeSpriteFaceAngle(param_sprite, param_angle_in_degrees) {
	param_sprite.rotation = param_angle_in_degrees * Math.PI / -180;
};

function makeTankFaceLeft(paramTank) {
	paramTank.scale.set(-paramTank.type.scale, paramTank.type.scale);
}

function makeTankFaceRight(paramTank) {
	paramTank.scale.set(paramTank.type.scale, paramTank.type.scale);
}

function getTankRegion(paramTank) {
	return {
		x1: parseInt(paramTank.position.x + paramTank.regionOffsetX - paramTank.regionWidth / 2),
		y1: parseInt(paramTank.position.y + paramTank.regionOffsetY - paramTank.regionHeight / 2),
		x2: parseInt(paramTank.position.x + paramTank.regionOffsetX + paramTank.regionWidth / 2),
		y2: parseInt(paramTank.position.y + paramTank.regionOffsetY + paramTank.regionHeight / 2),
		w: paramTank.regionWidth,
		h: paramTank.regionHeight
	};
}

var CreateTank = function CreateTank(param_TankType, param_position_x, param_position_y) {
	var TankSprite = new PIXI.Sprite(PIXI.utils.TextureCache[param_TankType.texture]);
	TankSprite.type = param_TankType;
	TankSprite.anchor.set(0.5, 0.5);
	TankSprite.scale.set(param_TankType.scale, param_TankType.scale);
	var TurretSprite = new PIXI.Sprite(PIXI.utils.TextureCache[param_TankType.turret.texture]);
	TurretSprite.scale.set(param_TankType.turret.scale, param_TankType.turret.scale);
	TurretSprite.anchor.set(param_TankType.turret.anchor.x, param_TankType.turret.anchor.y);
	makeSpriteFaceAngle(TurretSprite, 0);
	TankSprite.Turret = TurretSprite;
	TankSprite.Turret.offset = param_TankType.turret.offset;

	TankSprite.regionOffsetX = param_TankType.regionOffsetX;
	TankSprite.regionOffsetY = param_TankType.regionOffsetY;
	TankSprite.regionWidth = param_TankType.regionWidth;
	TankSprite.regionHeight = param_TankType.regionHeight;

	TanksContainer.addChild(TankSprite);
	TurretsContainer.addChild(TurretSprite);
	makeTankMove(TankSprite, param_position_x, param_position_y);
	sceneTanks.push(TankSprite);
	return TankSprite;
};

function initializeGravity() {
	var gravity = 8;
	var landing_smoothing = 6;
	PIXI_APP.ticker.add(function (delta) {
		if (sceneTanks.length >= 1) {
			for (var tankIndex in sceneTanks) {
				var TankRegion = getTankRegion(sceneTanks[tankIndex]);
				var TankVector = new SAT.Vector(TankRegion.x1, TankRegion.y1);
				var TankRegionBox = new SAT.Box(TankVector, TankRegion.w, TankRegion.h).toPolygon();

				var collidedWithAnything = false;
				var Y_Adjustment = 0;

				for (var regionIndex in sceneRegions) {
					var SceneRegion = sceneRegions[regionIndex];
					var SceneVector = new SAT.Vector(parseInt(SceneRegion.x - SceneRegion.w / 2), parseInt(SceneRegion.y - SceneRegion.h / 2));
					var SceneRegionBox = new SAT.Box(SceneVector, SceneRegion.w, SceneRegion.h).toPolygon();

					var TempSATResponse = new SAT.Response();
					if (SAT.testPolygonPolygon(TankRegionBox, SceneRegionBox, TempSATResponse)) {
						collidedWithAnything = true;
						if (TempSATResponse.overlap) {
							Y_Adjustment = TempSATResponse.overlap;
							//console.log(TempSATResponse.overlap);
						}
					}
				}

				if (collidedWithAnything) {
					if (Y_Adjustment) {
						makeTankMove(sceneTanks[tankIndex], sceneTanks[tankIndex].position.x, sceneTanks[tankIndex].position.y - Y_Adjustment / landing_smoothing);
					}
				} else {
					makeTankMove(sceneTanks[tankIndex], sceneTanks[tankIndex].position.x, sceneTanks[tankIndex].position.y + delta * gravity);
				}
			}
		}
	});
}

var KeyPressManager = new keypress.Listener();

function PixiAppHotkey(keys, param_function) {

	var TempTicker = new PIXI.ticker.Ticker();
	TempTicker.add(param_function);
	KeyPressManager.register_combo({
		"keys": keys,
		"on_keydown": function on_keydown() {
			TempTicker.start();
		},
		"on_keyup": function on_keyup() {
			TempTicker.stop();
		},
		"on_release": null,
		"this": undefined,
		"prevent_default": true,
		"prevent_repeat": true,
		"is_unordered": false,
		"is_counting": false,
		"is_exclusive": false,
		"is_solitary": false,
		"is_sequence": false
	});
}

console.log("Loader :: Queueing assets.");
PIXI.loader.add("./assets/spritesheets/sheet-1.json");
PIXI.loader.add("tile_winter_bg", "./assets/images/tile_winter_bg.png");

PIXI.loader.on("progress", function (loader, resource) {
	console.log("Loader :: Loading " + resource.url + " - " + loader.progress + "%");
});

var TempTank = null;

PIXI.loader.load(function (loader, resources) {

	console.log("Loader :: All assets loaded!");

	initializeCursor();
	initializeScene();
	initializeGravity();

	TempTank = CreateTank(TankTypes.DesertOne, windowWidth / 2, windowHeight * 0.8);

	PixiAppHotkey("a", function (delta) {
		makeTankMove(TempTank, TempTank.position.x - delta * 3, TempTank.position.y);
	});
	PixiAppHotkey("d", function (delta) {
		makeTankMove(TempTank, TempTank.position.x + delta * 3, TempTank.position.y);
	});
	PixiAppHotkey("w", function (delta) {
		makeTankMove(TempTank, TempTank.position.x, TempTank.position.y - delta * 12);
	});

	/*
 interactionManager.on("mousedown", function(data){
 	console.log(data);
 });
 */

	PIXI_APP.ticker.add(function (delta) {
		var angleDeg = Math.atan2(interactionManager.pointer.global.y - TempTank.Turret.position.y, interactionManager.pointer.global.x - TempTank.Turret.position.x) * 180 / Math.PI;
		var absAngleDeg = Math.abs(angleDeg);
		makeAimTankTurretToAngle(TempTank, absAngleDeg);
		if (absAngleDeg <= 90) {
			if (TempTank.facingDirection != "Right") {
				makeTankFaceRight(TempTank);
				TempTank.regionOffsetX = -Math.abs(TempTank.regionOffsetX);
			}
		} else {
			if (TempTank.facingDirection != "Left") {
				makeTankFaceLeft(TempTank);
				TempTank.regionOffsetX = Math.abs(TempTank.regionOffsetX);
			}
		}
	});
	/*
 setInterval(function(){
 	SceneContainer.position.set(
 		SceneContainer.position.x,
 		SceneContainer.position.y - 2
 	);
 	for(var regionIndex in sceneRegions){
 		sceneRegions[regionIndex].y = sceneRegions[regionIndex].y - 2;
 	}
 }, 300);
 */
	//Debug();
});

var Debug = function Debug() {
	// Create tracer
	// Trace all regions w/ pixi ticker..
	// tank bodies
	// projectile bodies
	// floors and walls
	var Tracer = new PIXI.Graphics();
	console.log("Debug :: Tracer started.");
	PIXI_APP.stage.addChild(Tracer);
	PIXI_APP.ticker.add(function (delta) {
		Tracer.clear();
		for (var regionIndex in sceneRegions) {

			Tracer.lineStyle(2, 0x77DD77, 1);

			Tracer.beginFill(0x77DD77, 1);
			Tracer.drawCircle(sceneRegions[regionIndex].x, sceneRegions[regionIndex].y, 2);
			Tracer.endFill();

			Tracer.drawRect(parseInt(sceneRegions[regionIndex].x - sceneRegions[regionIndex].w / 2), parseInt(sceneRegions[regionIndex].y - sceneRegions[regionIndex].h / 2), parseInt(sceneRegions[regionIndex].w), parseInt(sceneRegions[regionIndex].h));
		}

		for (var tankIndex in sceneTanks) {

			Tracer.lineStyle(2, 0x77DD77, 1);

			Tracer.beginFill(0x77DD77, 1);
			Tracer.drawCircle(sceneTanks[tankIndex].x, sceneTanks[tankIndex].y, 2);
			Tracer.endFill();

			var TempRect = getTankRegion(sceneTanks[tankIndex]);
			Tracer.drawRect(TempRect.x1, TempRect.y1, TempRect.w, TempRect.h);
		}
	});

	setTimeout(function () {
		PIXI_APP.stage.removeChild(Tracer);
		PIXI_APP.stage.addChild(Tracer);
	}, 100);
};

// PIXI-PARTICLES TEST:
function createEffect() {

	var emitterContainer = new PIXI.Container();
	PIXI_APP.stage.addChild(emitterContainer);

	var frames = [];
	frames.push(PIXI.Texture.fromImage("./assets/images/bullet1 (1).png"));
	frames.push(PIXI.Texture.fromImage("./assets/images/bullet1 (2).png"));
	frames.push(PIXI.Texture.fromImage("./assets/images/bullet1 (3).png"));
	frames.push(PIXI.Texture.fromImage("./assets/images/bullet1 (4).png"));

	var config = {
		"alpha": {
			"start": 1,
			"end": 0.25
		},
		"scale": {
			"start": 0.2,
			"end": 0.2
		},
		"color": {
			"start": "FFFFFF",
			"end": "FFFFFF"
		},
		"speed": {
			"start": 700,
			"end": 500
		},
		"startRotation": {
			"min": -85,
			"max": -95
		},
		"rotationSpeed": {
			"min": 0,
			"max": 0
		},
		"lifetime": {
			"min": 0.5,
			"max": 0.5
		},
		"frequency": 0.02,
		"emitterLifetime": 4,
		"maxParticles": 100,
		"pos": {
			"x": 0,
			"y": 0
		},
		"addAtBack": false,
		"spawnType": "point",
		"spawnCircle": {
			"x": 0,
			"y": 0,
			"r": 10
		},
		emit: true,
		autoUpdate: true
	};

	var ArtData = {
		framerate: 8,
		loop: true,
		textures: frames
	};

	var emitter = new PIXI.particles.Emitter(emitterContainer, frames, config);

	emitter.updateOwnerPos(window.innerWidth / 2, window.innerHeight / 2);

	setTimeout(function () {

		emitter.destroy();
		emitter = null;
		window.destroyEmitter = null;

		//reset SpriteRenderer's batching to fully release particles for GC
		if (PIXI_APP.renderer.plugins && PIXI_APP.renderer.plugins.sprite && PIXI_APP.renderer.plugins.sprite.sprites) {
			PIXI_APP.renderer.plugins.sprite.sprites.length = 0;
		}

		PIXI_APP.renderer.render(PIXI_APP.stage);
	}, 10000);
}
