
	browserify index.js -o panzer-mmo.js
	babel panzer-mmo.js --out-file panzer-mmo-babelified.js
	uglifyjs panzer-mmo-babelified.js --compress --mangle > panzer-mmo.min.js
	
	browserify pixi.umbrella.src.js -o pixi.umbrella.js