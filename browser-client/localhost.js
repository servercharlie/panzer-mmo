/**
 * Created by user on 1/18/2017.
 */

let express = require('express');
let path = require('path');
let app = express();

// Define the port to run on
app.set('port', 80);

app.use(express.static(path.join(__dirname, './dist/')));

// Listen for requests
let server = app.listen(app.get('port'), function() {
    let port = server.address().port;
    console.log(`\n\t/dist/ directory now accessible @ http://localhost:${port}/\n`);
});
