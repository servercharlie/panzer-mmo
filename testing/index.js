/**
 * Created by user on 1/18/2017.
 */

let express = require('express');
let path = require('path');
let app = express();

// Define the port to run on
app.set('port', 80);

app.use(express.static(path.join(__dirname, '../')));

// Listen for requests
let server = app.listen(app.get('port'), function() {
    let port = server.address().port;
    console.log(`Demos now accessible @ localhost port ${port}`);
});

app.get('/', function(req, res){
  res.redirect('/index');
});
app.get('/testing', function(req, res){
  res.redirect('/index');
});
app.get('/index', function(req, res){
  res.send(`
	<html>
		<title>Testing Index</title>
		<body>
			<a href="./testing/demo1">
				Demo # 1
			</a>
		</body>
	</html>
  `);
});
